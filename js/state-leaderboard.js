define(['consts', 'background', 'entities/shutter', 'engine/forms', 'leaderboards', 'engine/save-state'], function(Consts, Background, Shutter, Forms, Leaderboards, SaveState) {
	

	var state = new Phaser.State();

	state.create = function() {

		SaveState.victory = true;
		this.input.keyboard.clearCaptures();

		state.input.keyboard.clearCaptures();
		state.input.keyboard.reset(true);

		Leaderboards.fetch(function(data) {
			try {
				var highScore = false;
				if (data.length < 10) {
					highScore = true;
				}
				else {
					data.forEach(function(entry) {
						if (SaveState.points > entry.score) {
							highScore = true;
						}
					})
				}

				if (highScore) {
					state.actuallyCreate();
				}
				else {
					state.game.state.start('cutscene');
				}
			}
			catch (e) {
				console.error(e);
				state.game.state.start('cutscene');
			}
		})

	}

	state.actuallyCreate = function() {

		this.bg = Background.create(this);

		var logo = this.add.image(Consts.WIDTH / 2, 130, 'logo');
		logo.anchor.set(0.5);
		//logo.angle = -3;

		state.sound.stopAll();
		if (!SaveState.mute) {
			state.add.audio('victory').play('', 0, 1);
		}

		state.form = Forms.load('leaderboard', SaveState);
		state.form.show();
		state.form.getElement('submit').click(function() {
			state.form.hide();
			SaveState.playerName = state.form.getElement('name').val();

			// Please don't hack us! http://i.imgur.com/6CYdOcW.jpg
			Leaderboards.submitScore(SaveState.playerName, SaveState.points, function() {
				state.form.hide();
				state.game.state.start('cutscene');
			})
		});
		state.form.getElement('cancel').click(function() {
			state.form.hide();
			state.game.state.start('cutscene');
		});
		state.inputEnabled = true;

	};

	state.update = function() {

	}

	return state;

});
