define(['consts'], function(Consts) {

	var BOTTOM_RATIO = 0.13;
	var TOP_RATIO = 0.35;

	var Background = function(state) {
		this.bottom = [state.add.image(0, 0, 'bgbottom'), state.add.image(0, 0, 'bgbottom')];
		this.top = [state.add.image(0, 0, 'bgtop'), state.add.image(0, 0, 'bgtop')];
		
		this.bottom[0].fixedToCamera = true;
		this.bottom[1].fixedToCamera = true;
		this.top[0].fixedToCamera = true;
		this.top[1].fixedToCamera = true;
	};

	Background.prototype.offset = function(amountX, amountY) {
		var bottomPosX = (-amountX * BOTTOM_RATIO % Consts.WIDTH);
		var topPosX = (-amountX * TOP_RATIO % Consts.WIDTH);
		this._update(this.bottom, bottomPosX, amountY);
		this._update(this.top, topPosX, amountY);
	}

	Background.prototype._update = function(sprites, posX, posY) {
		sprites.forEach(function(sprite) {
			sprite.cameraOffset.x = posX;
			// sprite.y = posY;
			posX += Consts.WIDTH;
		})
	}

	return {

		create: function create(state) {
			return new Background(state);
		}

	}
	
})