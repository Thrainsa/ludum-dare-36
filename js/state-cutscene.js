define(['consts', 'background', 'entities/shutter', 'engine/forms', 'engine/tilemap', 'engine/chain', 'engine/save-state', 'entities/player', 'entities/monster'], 
		function(Consts, Background, Shutter, Forms, Tilemap, Chain, SaveState, FactoryPlayer, FactoryMonster) {

	var state = new Phaser.State();

	state.preload = function() {
		this.levelInfo = {};
		this.levelInfo.tilesets = ["tileset", "objects"];

		if(SaveState.cutscene == 'start'){
			this.load.tilemap('cutscene-start', 'tilemaps/maps/cutscene-start.json', null, Phaser.Tilemap.TILED_JSON);
		} else if(SaveState.cutscene == 'beforeBoss' || SaveState.cutscene == 'victory'){
			this.load.tilemap('level3-1', 'tilemaps/maps/level3-1.json', null, Phaser.Tilemap.TILED_JSON);
		}
		
	};

	state.create = function() {
		this.game.physics.startSystem(Phaser.Physics.P2JS);
		this.bg = Background.create(this);

		if(SaveState.cutscene == 'start'){
			this.createStartCutscene();
		} else if(SaveState.cutscene == 'beforeBoss'){
			this.createBeforeBossCutscene();
		} else if(SaveState.cutscene == 'victory'){
			this.createVictoryCutscene();
		}
		
		this.chain.start();

	};
	
	state.createStartCutscene = function(){
		this.groups = {
			map: this.game.physics.p2.createCollisionGroup(),
			player: this.game.physics.p2.createCollisionGroup(),
			robot: this.game.physics.p2.createCollisionGroup(),
			enemies: this.game.physics.p2.createCollisionGroup(),
			obstacles: this.game.physics.p2.createCollisionGroup(),
			points: this.game.physics.p2.createCollisionGroup(),
			projectile: this.game.physics.p2.createCollisionGroup(),
			sensor: this.game.physics.p2.createCollisionGroup()
		};
		var groupsToCollideWith = [
			state.groups.player,
			state.groups.enemies
		];
		
		var map = Tilemap.load(this, 'cutscene-start', groupsToCollideWith);
		state.displaygroups = {};
		state.displaygroups.objects = state.add.group();
		
		var playerData = this.levelInfo.objects['player'][0];
		var player = FactoryPlayer.create(this, playerData);
		
		player.actionMap.isDown = function(){return false;};
		player.leftButton = {};
		player.body.x = 50;
		
		this.chain = Chain.create();

		this.chain.add(function(next) {
			state.shutter = Shutter.create(state);
			state.shutter.cutsceneMode(function() {
				setTimeout(next, 250 );
			});
	    	state.musicSound = state.add.audio('theme');
	    	state.musicSound.fadeIn(2000, true);
		});

		state.levelInfo.escKey = state.game.input.keyboard.addKey(Phaser.Keyboard.ESC);
		state.levelInfo.escKey.onDown.addOnce(function() {
			state.chain.kill();
			state.musicSound.fadeOut(1000);
			if (state.form) {
				state.form.unload();
			}
			state.shutter.close(function() {
				state.game.state.start('level');
			});
		});
		
		this.chain.add(function(next) {
			player.actionMap.isDown = function(action){if(action == 3){return true;} else {return false;}};
			setTimeout(next, 700);
		});
		
		this.chain.add(function(next) {
			player.actionMap.isDown = function(action){return false;};
			player.scale.x *=-1;
			
			state.form = Forms.load('cutscene');
			state.form.hide();
			setText("LD36: Don't run that fast Techsa, you known it's not safe up here.", 4500, next);
		});

		this.chain.add(function(next) {
			setText("Techsa: Don't be that shy LD36, we need to hurry and find this ancient generator you told me about.", 7000, next);
		});
		
		this.chain.add(function(next) {
			setText("Techsa: Our village will not survive any longer if we can't maintain our shield running up.", 6500, next);
		});
		
		this.chain.add(function(next) {
			setText("Techsa: Hell knows what sort of monster can be on the other side of the shield", 5000, next);
		});
		
		this.chain.add(function(next) {
			setText("LD36: I alrealdy know that, but I have a bad fealling about this journey in the forbidden forest.", 6500, next);
		});
		
		this.chain.add(function(next) {
			setText("LD36: No one have been that far for centuries and this generator I have in memory might not be there anymore.", 7000, next);
		});
		
		this.chain.add(function(next) {
			setText("Techsa: Maybe, but there is only one way to find out. Come one!", 4000, next);
		});


		this.chain.add(function(next) {
			player.actionMap.isDown = function(action){if(action == 3){return true;} else {return false;}};
			state.musicSound.fadeOut(1000);
			state.form.hide(200, next);
		});

		this.chain.add(function(next) {
			state.shutter.close(next);
		});
		
		this.chain.add(function(next) {
			state.game.state.start('level');
		});
	}
	
	state.createBeforeBossCutscene = function(){
		this.bossFilter = state.add.filter('Boss');
		this.groups = {
			map: this.game.physics.p2.createCollisionGroup(),
			player: this.game.physics.p2.createCollisionGroup(),
			robot: this.game.physics.p2.createCollisionGroup(),
			enemies: this.game.physics.p2.createCollisionGroup(),
			obstacles: this.game.physics.p2.createCollisionGroup(),
			points: this.game.physics.p2.createCollisionGroup(),
			projectile: this.game.physics.p2.createCollisionGroup(),
			sensor: this.game.physics.p2.createCollisionGroup()
		};
		var groupsToCollideWith = [
			state.groups.player,
			state.groups.enemies
		];
		
		var map = Tilemap.load(this, 'level3-1', groupsToCollideWith);
		state.displaygroups = {};
		state.displaygroups.objects = state.add.group();
		
		var playerData = this.levelInfo.objects['player'][0];
		var player = FactoryPlayer.create(this, playerData);
		this.player = player;
		
		state.game.camera.y = player.y;
		
		var objects = this.levelInfo.objects['monster'];
		for (var i=0; i < objects.length; i++) {
			var data = objects[i];
			var m = FactoryMonster.create(state, data);
			m.update = function(){};
			state.displaygroups.objects.add(m);
		}
	
		
		player.actionMap.isDown = function(){return false;};
		player.leftButton = {};
		player.body.x = 50;
		
		this.chain = Chain.create();

		this.chain.add(function(next) {
			state.shutter = Shutter.create(state);
			state.shutter.cutsceneMode(function() {
				setTimeout(next, 250 );
			});
	    	state.musicSound = state.add.audio('theme');
	    	state.musicSound.fadeIn(2000, true);
		});

		state.levelInfo.escKey = state.game.input.keyboard.addKey(Phaser.Keyboard.ESC);
		state.levelInfo.escKey.onDown.addOnce(function() {
			state.chain.kill();
			state.musicSound.fadeOut(1000);
			if (state.form) {
				state.form.unload();
			}
			state.shutter.close(function() {
				state.game.state.start('level');
			});
		});
		
		this.chain.add(function(next) {
			player.actionMap.isDown = function(action){if(action == 3){return true;} else {return false;}};
			setTimeout(next, 300);
		});
		
		this.chain.add(function(next) {
			player.actionMap.isDown = function(action){return false;};
			player.scale.x *=-1;
			
			state.form = Forms.load('cutscene');
			state.form.hide();
			setText("LD36: Stop Techsa, I recognize the place. The generator is near I'm sure of it.", 4500, next);
		});

		this.chain.add(function(next) {
			setText("Techsa: Look LD36! Have you seen this giant robot!", 4000, next);
			var tween = state.add.tween(state.game.camera).to({x: 1050}, 2000);
			tween.start();
		});
		
		this.chain.add(function(next) {
			setText("LD36: Yes! It's him! He is the one that keep the generator.", 4000, next);
		});
		
		this.chain.add(function(next) {
			setText("Techsa: What!! You didn't mention that before! You knew that I would have to fight a giant robot?", 6000, next);
		});
		
		this.chain.add(function(next) {
			setText("LD36: I didn't know he would still be there. But, I'm sure you can defeat him. Good luck", 5500, next);
		});

		this.chain.add(function(next) {
			state.musicSound.fadeOut(1000);
			state.form.hide(200, next);
		});

		this.chain.add(function(next) {
			state.shutter.close(next);
		});
		
		this.chain.add(function(next) {
			state.game.state.start('level');
		});
		
	}
	
	state.createVictoryCutscene = function(){
		this.bossFilter = state.add.filter('Boss');
		this.groups = {
			map: this.game.physics.p2.createCollisionGroup(),
			player: this.game.physics.p2.createCollisionGroup(),
			robot: this.game.physics.p2.createCollisionGroup(),
			enemies: this.game.physics.p2.createCollisionGroup(),
			obstacles: this.game.physics.p2.createCollisionGroup(),
			points: this.game.physics.p2.createCollisionGroup(),
			projectile: this.game.physics.p2.createCollisionGroup(),
			sensor: this.game.physics.p2.createCollisionGroup()
		};
		var groupsToCollideWith = [
			state.groups.player,
			state.groups.enemies
		];
		
		var map = Tilemap.load(this, 'level3-1', groupsToCollideWith);
		state.displaygroups = {};
		state.displaygroups.objects = state.add.group();
		
		var playerData = this.levelInfo.objects['player'][0];
		var player = FactoryPlayer.create(this, playerData);
		this.player = player;
		
		state.game.camera.y = player.y;
		
		var objects = this.levelInfo.objects['monster'];
		for (var i=0; i < objects.length; i++) {
			var data = objects[i];
			if(data.properties.boss == "true"){
				var m = FactoryMonster.create(state, data);
				m.update = function(){};
				m.dead = true;
				state.displaygroups.objects.add(m);
			}
		}
	
		
		player.actionMap.isDown = function(){return false;};
		player.leftButton = {};
		player.body.x = 1500;
		player.robot.body.x = 1400;
		state.game.camera.x = 1200;
		
		var generator = this.add.image(player.body.x + 100, player.body.y + 50, 'generator');
		state.add.tween(generator.scale)
			.to({x: 0.9, y: 0.9}, 800,
				Phaser.Easing.Quadratic.InOut, true, 0, -1, true);
		
		this.chain = Chain.create();

		this.chain.add(function(next) {
			state.shutter = Shutter.create(state);
			state.shutter.cutsceneMode(function() {
				setTimeout(next, 250 );
			});
	    	state.musicSound = state.add.audio('theme');
	    	state.musicSound.fadeIn(2000, true);
		});

		state.levelInfo.escKey = state.game.input.keyboard.addKey(Phaser.Keyboard.ESC);
		state.levelInfo.escKey.onDown.addOnce(function() {
			state.chain.kill();
			state.musicSound.fadeOut(1000);
			if (state.form) {
				state.form.unload();
			}
			state.shutter.close(function() {
				state.game.state.start('menu');
			});
		});
		
		this.chain.add(function(next) {			
			state.form = Forms.load('cutscene');
			state.form.hide();
			setText("Techsa: Finaly! It's Dead!", 3000, next);
		});

		this.chain.add(function(next) {
			setText("LD36: Look we've found the generator!", 4000, next);
			// Pop du generateur
			var tween = state.add.tween(state.game.camera).to({x: 1050}, 2000);
			tween.start();
		});
		
		this.chain.add(function(next) {
			setText("Techsa: Yes! We can go home now. Quick LD36! Follow me!", 4000, next);
		});
		
		this.chain.add(function(next) {
			player.actionMap.isDown = function(action){if(action == 1){return true;} else {return false;}};
			var tween = state.add.tween(generator).to({x: 900}, 1000);
			tween.start();
			setTimeout(next, 1000);
		});

		this.chain.add(function(next) {
			state.musicSound.fadeOut(1000);
			state.form.hide(200, next);
		});

		this.chain.add(function(next) {
			state.shutter.close(next);
		});
		
		this.chain.add(function(next) {
			state.game.state.start('menu');
		});
		
	}

	function setText(text, delay, callback) {
		state.form.hide(200, function() {
			state.form.getElement('text').html(text);
			setTimeout(function() {
				state.form.show(200, function() {
					setTimeout(callback, delay);
				});
			}, 500);
		})
	}

	return state;

});
