define(['consts', 'engine/utils'], function(Consts, Utils) {
	return {
		create: create
	};

	function create(state, data) {
		var point = state.game.add.sprite(data.x, data.y, 'energy-idle');
		point.animations.add("idle");
		point.animations.play("idle", 4, true);
		if (data.properties.scale) {
			data.properties.scale = parseInt(data.properties.scale);
            point.scale = new PIXI.Point(data.properties.scale, data.properties.scale);
        }
		data.properties.value = parseInt(data.properties.value);
		point.info = data;
		point.state = state;
		
		state.game.physics.p2.enable(point, Consts.DEBUG.SHOW_BODIES);
		
		point.body.setCollisionGroup(state.groups.points);
		point.body.collides(state.groups.player);
		point.body.onBeginContact.add(playerCollision, point);

		// correction position body
		Utils.replaceSprite(point);
		point.body.data.gravityScale = 0;
	
		point.body.data.shapes[0].sensor=true
	
		// Animation
		
		// Audio

		point.update = function() {
		};
		
		point.getValue = function(){
			return data.properties.value;
		}

		return point;
	}
	
		function playerCollision(playerBody){
			this.state.displaygroups.objects.remove(this);
			this.state.world.add(this);
			this.body.destroy();
			this.body = null;
			this.fixedToCamera = true;
			this.cameraOffset.x -= this.game.camera.x;
			this.cameraOffset.y -= this.game.camera.y;
			
			
			var xInterpol = [this.cameraOffset.x , this.cameraOffset.x * 0.9, this.cameraOffset.x * 0.65, this.cameraOffset.x * 0.35, 50];
			var yInterpol = [this.cameraOffset.y, this.cameraOffset.y * 0.65, this.cameraOffset.y * 0.35, this.cameraOffset.y * 0.10, 50];
			var tween = this.game.add.tween(this.cameraOffset).to( {
				x: xInterpol,
				y: yInterpol
			}, 1100);
			tween.interpolation(function(v, k){
				return Phaser.Math.bezierInterpolation(v, k);
			});
			tween.onComplete.add(function(){
				playerBody.sprite.collectPoints(this);
			}, this);
			tween.start();
		}
});
