define(['consts', 'engine/utils'], function(Consts, Utils) {
	return {
		preload: preload,
		create: create
	};
	
	function preload(state) {
	}

	function create(state, player) {

		
		var robot = state.game.add.sprite(player.body.x - 100, player.body.y-70, 'robot-idle');
		state.game.physics.p2.enable(robot, Consts.DEBUG.SHOW_BODIES);
		robot.body.fixedRotation = true;
		robot.body.setCollisionGroup(state.groups.robot);
		robot.player = player;
		robot.body.data.gravityScale = 0;
		robot.body.damping = 0.9;
		
		robot.maxYSpeed = 200;
		
		// Audio

		// IA
		robot.update = function(){
			var xDiff = this.player.body.x - this.body.x;
			var yDiff = this.player.body.y - this.body.y;
			var distanceX = Math.abs(xDiff);
			var distanceY = Math.abs(yDiff);
			var direction = xDiff > 0 ? 1 : -1;
			var playerDirection = this.player.body.velocity.x > 0 ? 1 : -1;
			
			this.body.velocity.x = Math.cos(this.player.info.state.game.time.now / 500.0) * 20;  
			if(Math.abs(this.player.body.velocity.x) > 0 && distanceX > 100 && playerDirection == direction){
				this.body.velocity.x += this.player.body.velocity.x;
			}
			if(distanceX > 100){
				this.body.velocity.x += direction * (distanceX - 100)/5;
			}
			
			this.body.velocity.y = Math.sin(this.player.info.state.game.time.now / 500.0) * 20;  

			
			if(yDiff < 0){
				this.body.velocity.y -= this.maxYSpeed;
			} else if(yDiff < 100){
				this.body.velocity.y -= this.maxYSpeed - 2 * yDiff;
			} else if(yDiff > 150 && yDiff < 300){
				this.body.velocity.y += this.maxYSpeed - 2 * (150 + this.maxYSpeed/2 - Math.min(yDiff,150+this.maxYSpeed/2));
			} else if(yDiff > 250){
				this.body.velocity.y += this.maxYSpeed * 3;
			}
			if(playerDirection == direction){
				this.scale.x = this.player.scale.x;
			}
		}
		
		robot.switchStance = function(stance) {
			if (stance != this.currentStance) {
				Utils.switchStance(this, stance);
				this.body.setCollisionGroup(state.groups.robot);
			}
		}
		
		// Animations
		robot.switchStance(Consts.ROBOT.STANCES["idle"]);
		
		return robot;
	}
});
