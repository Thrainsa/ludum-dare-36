define(['consts', 'engine/utils', 'entities/sensor'], function(Consts, Utils, FactorySensor) {
	return {
		create: create
	};

	function create(state, data) {
		var secret = FactorySensor.create(state, data);
		var layer = state.levelInfo.layers['secretCache'];
		
		secret.body.onBeginContact.add(function(){
			secret.playerIn = true;
			this.alpha = 0.6;
		}, layer );
		
		secret.update = function(){
			if(secret.playerIn){
				var distPlayerX = state.player.body.x - this.body.x;
				var distPlayerY = state.player.body.y - this.body.y;
				if(Math.abs(distPlayerX) > secret.data.width/2 + Math.abs(state.player.width)/2
					|| Math.abs(distPlayerY) > secret.data.height/2 + Math.abs(state.player.height)/2){
					layer.alpha = 1;
					secret.playerIn = false;
				}
			}
		};

		return secret;
	}

});
