define(['consts', 'engine/utils', 'entities/IA/ROBOT', 'entities/IA/ROBOT_ZOMBIE'], function(Consts, Utils, RobotFactory, RobotZombieFactory) {
	return {
		create: create
	};

	function create(state, data) {
		var monster = state.game.add.sprite(data.x, data.y, Consts.ENEMY[data.properties.name].STANCES["idle"].texture);
		if (data.properties.scale) {
            monster.scale = new PIXI.Point(data.properties.scale, data.properties.scale);
        }
		
		monster.info = {
			name: data.properties.name,
			direction : data.properties.direction || "left",
			invincible: data.properties.invincible == "true",
			startX: data.x,
			startY: data.y,
			dead: false,
			health: data.properties.health || 0,
			properties: data.properties
		};
		
		// correction position body
		Utils.attachObjectToGrid(monster,true, Consts.TILESIZE);
		
		state.game.physics.p2.enable(monster, Consts.DEBUG.SHOW_BODIES);
		monster.body.fixedRotation = true;
		monster.body.setCollisionGroup(state.groups.enemies);
		monster.body.collides(state.groups.player);
		monster.body.collides(state.groups.map);
		
		// correction position body
		Utils.replaceSprite(monster);
		monster.body.mass = 50;

		// Animation	
		
		if (data.properties.gravity !== undefined) {
            monster.body.data.gravityScale = parseInt(data.properties.gravity);
        }
		
		if(monster.info.direction == "left"){
			monster.scale.x *= -1;
		}
		
		// Audio

		// IA
		if(data.properties.name == "ROBOT"){
			monster.ia = RobotFactory.create(state, monster);
		} else if(data.properties.name == "ROBOT_ZOMBIE"){
			monster.ia = RobotZombieFactory.create(state, monster);
		}
		
		
		monster.update = function() {
			if(this.ia){
				this.ia.update();
			}	
		};
		
		monster.wounded = function(dommage){
			if(this.ia && this.ia.wounded){
				this.ia.wounded();
			} else {
				this.info.health -= dommage;
				if(this.info.health <= 0){
					this.death();
				}
			}
		}
		
		monster.death = function(){
			if(!this.dead ){
				this.dead = true;
				if(this.ia){
					this.ia.death();
				}
			}
		}
		
		monster.switchStance = function(stance) {
			if (stance != this.currentStance) {
				Utils.switchStance(this, stance);
				
				if(this.ia && this.ia.switchStance){
					this.ia.switchStance(stance);
				}
				
				this.body.setCollisionGroup(state.groups.enemies);
				this.body.collides(state.groups.player);
				this.body.collides(state.groups.map);
				this.body.collides(state.groups.projectile);
			}
			

		}
		
		// Animations
		if(monster.ia && monster.ia.initFirstAnimation){
			monster.ia.initFirstAnimation();
		} else {
			monster.switchStance(Consts.ENEMY[data.properties.name].STANCES["idle"]);
		}
		
		
		
		return monster;
	}
});
