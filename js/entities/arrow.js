define(['consts', 'engine/utils'], function(Consts, Utils) {
	return {
		create: create
	};
	
	function create(arrowType, state, player) {
		if(arrowType == "arrow"){
			return createArrow(state, player);
		} else if(arrowType == "arrow-energy"){
			return createArrowEnergy(state, player);
		} else {
			throw "No arrow of type: "+arrowType;
		}
	}
	
	function createArrow(state, player) {
		var arrow = init(state, player, 'arrow');
		arrow.maxLoadingTime = 0.9;
		arrow.nextType='arrow-energy';
		return arrow;
	}
	
	function createArrowEnergy(state, player) {
		var arrow = init(state, player, 'arrow-energy');
		arrow.maxLoadingTime = 0.9;
		arrow.energyUsed = 0;
		arrow.custom.bend = function() {
			player.useEnergy(5);
		};
		arrow.custom.update = function() {
			if(arrow.bendStartTime){
				var loadingTime = arrow.game.time.now - arrow.bendStartTime;
				var eneryNeeded = Math.round(Math.min(loadingTime / 1000, arrow.maxLoadingTime) * 10);
				if(arrow.energyUsed < eneryNeeded){
					player.useEnergy(eneryNeeded - arrow.energyUsed);
					arrow.energyUsed = eneryNeeded;
				}
			}
		};
		arrow.info.dommage.min = 5;
		arrow.info.dommage.max = 10;
		return arrow;
	}

	function init(state, player, spriteName) {
		var arrow = state.game.add.sprite(player.body.x -10, player.body.y -27, spriteName);
		arrow.custom = {};
		state.displaygroups.objects.add(arrow);
		arrow.info = {
			dommage: {
				min : 1,
				max : 3
			},
			state : state
		};
		arrow.shot = false;
		arrow.scale.x = 0;
		arrow.pivot.x = player.pivot.x;

		arrow.update = function() {
			if(!this.collide){
				if(this.shot && this.body.velocity.x != 0 ){
					this.body.rotation = Math.atan2(this.body.velocity.y, this.body.velocity.x);
				}
			}
			if(this.custom.update){
				this.custom.update();
			}
			if(!arrow.loaded){
				var loadingTime = this.game.time.now - this.bendStartTime;
				if(loadingTime >= arrow.maxLoadingTime * 1000){
					arrow.loaded = true;
				}
			}
		};
		
		arrow.bend = function() {
			this.bendStartTime = this.game.time.now;
			if(this.custom.bend){
				this.custom.bend();
			}
		};
		
		arrow.shoot = function(event) {
			this.scale.x = 1;
			this.info.state.physics.p2.enable(arrow, Consts.DEBUG.SHOW_BODIES);
		
			this.body.setCollisionGroup(state.groups.projectile);
			this.body.collides(state.groups.map, collideWithMap);
			this.body.collides(state.groups.enemies, collideWithEnemy);
			this.body.collides(state.groups.obstacles);
		
			var loadingTime = this.game.time.now - this.bendStartTime;
			this.bendStartTime = false;
			
			var force = 50 * Consts.GRAVITY;
			force += force * Math.min(arrow.maxLoadingTime, loadingTime / 1000);
			var xDiff = event.worldX - this.body.x;
			var yDiff = event.worldY - this.body.y;
			var angle = Math.atan2(yDiff, xDiff);
			
			arrow.body.force.x = Math.cos(angle) * force;    // accelerateToObject 
			arrow.body.force.y = Math.sin(angle) * force;
			arrow.body.rotation = Math.atan2(arrow.body.force.y, arrow.body.force.x);
			if (typeof arrow.energyUsed == 'undefined'){
				arrow.dommage = Math.round(arrow.info.dommage.min +  Math.min(arrow.maxLoadingTime, loadingTime / 1000) / arrow.maxLoadingTime * (arrow.info.dommage.max - arrow.info.dommage.min));
			}
			else {
				var maxNeeded = arrow.maxLoadingTime * 10;
				if(arrow.energyUsed < maxNeeded){
					arrow.dommage = arrow.info.dommage.min + Math.round(arrow.energyUsed * (arrow.info.dommage.max - arrow.info.dommage.min) / maxNeeded);
				} else {
					arrow.dommage = arrow.info.dommage.max
				}
			}
			this.shot = true;
		};

		return arrow;
	}
	
	function collideWithMap(arrowBody, collisionBody){
		arrowBody.sprite.collide = true;
		arrowBody.setZeroForce();
		arrowBody.setZeroVelocity();
		arrowBody.fixedRotation = true;
		arrowBody.static = true;
		setTimeout(function(){
			if(arrowBody.sprite != null){
				arrowBody.sprite.destroy();
			}
		}, 3000);
	}
	
	function collideWithEnemy(arrowBody, collisionBody){
		arrowBody.sprite.collide = true;
		var arrowSprite = arrowBody.sprite;

		if(collisionBody.sprite){
			arrowBody.destroy();
			collisionBody.setZeroForce();
			collisionBody.setZeroVelocity();

			arrowSprite.position.x -= collisionBody.sprite.position.x;
			arrowSprite.position.y -= collisionBody.sprite.position.y;
			collisionBody.sprite.addChild(arrowSprite);
			if(collisionBody.sprite.scale.x < 0){
				arrowSprite.position.x *= -1
				arrowSprite.rotation = -arrowSprite.rotation + 3.14
			}

			if(collisionBody.sprite.wounded){
				collisionBody.sprite.wounded(arrowSprite.dommage);
			}
			
			if(Math.abs(collisionBody.sprite.scale.x) > 1){
				arrowSprite.position.x /= Math.abs(collisionBody.sprite.scale.x);
				arrowSprite.position.y /= Math.abs(collisionBody.sprite.scale.y);
				arrowSprite.scale.x /= Math.abs(collisionBody.sprite.scale.x);
				arrowSprite.scale.y /= Math.abs(collisionBody.sprite.scale.y);
			}
		}
	}
	

});
