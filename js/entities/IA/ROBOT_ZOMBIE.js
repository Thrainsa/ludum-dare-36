define(['consts', 'entities/laser'], function(Consts, LaserFactory) {
	return {
		create: create
	};

	function create(state, monster) {
		var ia = {
			monster: monster,
			state: state,
			playerInRange: false,
			moving: false,
			activated: false,
			fireRate: 3, // nb Sec between shoots
			lastShot: 1
		};
		
		if(monster.info.properties.boss == "true"){
			monster.scale.x *= 2;
			monster.scale.y *= 2;
			monster.filters = [state.bossFilter];
			ia.fireRate = 2;
		}

		
		monster.body.onBeginContact.add(enterInRangeCallback, monster );
		// monster.body.onEndContact.add(leaveRangeCallback, monster );
		
		// IA
		ia.initFirstAnimation = function() {
			monster.switchStance(Consts.ENEMY[monster.info.name].STANCES["activate"]);
			monster.animations.play("sleeping", Consts.ENEMY[monster.info.name].STANCES["activate"].fps, true);
		}
		
		ia.update = function() {
			if(!this.monster.dead && this.activated && this.monster.body){
				if(this.playerInRange && this.canShoot()){
					this.shoot();
				} else if(this.moving){
					var distStartX = this.monster.body.x - this.monster.info.startX;
					var distPlayerX = this.state.player.body.x - this.monster.body.x;
					if(Math.abs(distPlayerX) > 550){
						this.playerInRange = false;
						// Stop moving animations
						this.stop();
						return;
					}
					// On ne s'éloigne pas trop de la zone de départ
					if(Math.abs(distStartX) < 500 || Math.abs(distStartX + distPlayerX) < 500){
						if(distPlayerX > 0){
							if(monster.info.direction != "right"){
								monster.info.direction = "right";
								monster.scale.x *= -1;
							}
							this.monster.body.velocity.x = 50;
						} else if(distPlayerX < 0){
							if(monster.info.direction != "left"){
								monster.info.direction = "left";
								monster.scale.x *= -1;
							}
							this.monster.body.velocity.x = -50;
						}
					} else {
						this.stop();
					}
				}
			}
		};
		
		ia.death = function(){
			if(this.shootAnimation){
				this.shootAnimation.stop(true);
			}
			this.monster.body.velocity.x = 0;
			this.moving = false;
			this.monster.switchStance(Consts.ENEMY[this.monster.info.name].STANCES["death"]);
			this.monster.animations.play("death", Consts.ENEMY[this.monster.info.name].STANCES["death"].fps, false);
			
			if(this.monster.info.properties.boss == "true"){
				this.monster.animations.currentAnim.onComplete.add(function(){
					this.state.victory();
				}, this);
			}
		}
		
		ia.switchStance = function(stance) {
			var rangeSensor =  this.monster.body.addRectangle(1000,450,0,-125);
			rangeSensor.sensor = true;
		}
		
		ia.stop = function(){
			this.monster.body.velocity.x = 0;
			this.moving = false;
			this.monster.switchStance(Consts.ENEMY[this.monster.info.name].STANCES["idle"]);
			this.monster.animations.play("idle", Consts.ENEMY[this.monster.info.name].STANCES["idle"].fps, true);
		}
		
		ia.startMoving = function(){
			this.moving = true;
			this.monster.switchStance(Consts.ENEMY[this.monster.info.name].STANCES["walk"]);
			this.monster.animations.play("walk", Consts.ENEMY[this.monster.info.name].STANCES["walk"].fps, true);
		}
		
		ia.activate = function(){
			if(!this.startActivation){
				this.startActivation = true;
				this.monster.switchStance(Consts.ENEMY[this.monster.info.name].STANCES["activate"]);
				this.monster.animations.play("activate", Consts.ENEMY[this.monster.info.name].STANCES["activate"].fps, false);
				this.monster.animations.currentAnim.onComplete.add(function(){
					this.activated = true;
					this.startMoving();
				}, this);
			}

		}
		
		ia.startShooting = function(){
			this.monster.body.velocity.x = 0;
			this.moving = false;
			this.monster.switchStance(Consts.ENEMY[this.monster.info.name].STANCES["shoot"]);
			this.monster.animations.play("prepare", Consts.ENEMY[this.monster.info.name].STANCES["shoot"].fps, false);
			flashShootAnimation(this.state, ia.monster, ia.monster.body.x + 10 * ia.monster.scale.x, ia.monster.body.y -65);
		}
		
		ia.shoot = function(){
			this.lastShot = false;
			this.startShooting();
			var distPlayerX = this.state.player.body.x - this.monster.body.x;
			var direction = distPlayerX < 0 ? -1 : 1;
			setTimeout(function(){
				if(!ia.monster.dead && ia.monster.body){
					var newDistPlayerX = ia.state.player.body.x - ia.monster.body.x;
					var newDirection = newDistPlayerX < 0 ? -1 : 1;
					if(direction == newDirection){
						ia.monster.animations.play("shoot", Consts.ENEMY[ia.monster.info.name].STANCES["shoot"].fps, false);
						var laser = LaserFactory.create(ia.monster, ia.state, ia.monster.body.x + 35 * ia.monster.scale.x, ia.monster.body.y -50);
						laser.shoot(ia.state.player.body.x, ia.state.player.body.y);
					}
					ia.lastShot = ia.state.game.time.now;
					ia.startMoving();
				}
			}, 2000);
		}
		
		ia.canShoot = function(){
			return this.lastShot && (this.state.game.time.now - this.lastShot) > this.fireRate * 1000;
		}
		
		return ia;
	}
	
	function enterInRangeCallback(p2Body, body, fixture1, fixture2, begin) {
		if(!this.dead && !this.ia.playerInRange && p2Body === this.ia.state.player.body){
			if(!this.ia.activated){
				this.ia.activate();
			} else{
				this.ia.playerInRange = true;
				// Start moving animations
				this.ia.startMoving();
			}

		}
	}
	
	
	function flashShootAnimation(state, monster,x, y) {
		// animation
		var flash = state.add.sprite(8 , -63, 'loading-flash');
		monster.addChild(flash);
		flash.anchor.set(0.5);
		flash.alpha = 0;
		flash.scale.x = 1.2;
		flash.scale.y = 1.2;
		var tween = state.add.tween(flash).to({alpha: 1, rotation: 50}, 2000)
		tween.onComplete.add(function(){
			flash.destroy();
			monster.ia.shootAnimation = null;
		});
		tween.start();
		monster.ia.shootAnimation = tween;
	}


	
});
