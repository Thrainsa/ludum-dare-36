define(['consts', 'entities/laser'], function(Consts, LaserFactory) {
	return {
		create: create
	};

	function create(state, monster) {
		var ia = {
			monster: monster,
			state: state,
			playerInRange: false,
			fireRate: 2.5, // nb Sec between shoots
			lastShot: 1,
			shooting: false
		};
		
		monster.body.onBeginContact.add(enterInRangeCallback, monster );

		// IA
		ia.update = function() {
			if(!this.monster.dead  && this.monster.body){
				if(this.playerInRange && this.canShoot()){
					this.shoot();
				} else if(!this.shooting){
					this.monster.body.velocity.y = Math.sin(this.state.game.time.now / 2000.0) * 60;
					
					var distPlayerX = this.state.player.body.x - this.monster.body.x;
					if(Math.abs(distPlayerX) > 550){
						this.playerInRange = false;
						return;
					}
					if(distPlayerX > 0){
							if(monster.info.direction != "right"){
								monster.info.direction = "right";
								monster.scale.x *= -1;
							}
						} else if(distPlayerX < 0){
							if(monster.info.direction != "left"){
								monster.info.direction = "left";
								monster.scale.x *= -1;
							}
						}
				}
				
			}	

		};
		
		ia.death = function(){
			this.monster.body.data.gravityScale = 1;
		}

		ia.switchStance = function(stance) {
			var rangeSensor =  this.monster.body.addRectangle(800,400,0,0);
			rangeSensor.sensor = true;
		}
		
		ia.shoot = function(){
			this.lastShot = false;
			this.startShooting();
			var distPlayerX = this.state.player.body.x - this.monster.body.x;
			var direction = distPlayerX < 0 ? -1 : 1;
			setTimeout(function(){
				if(!ia.monster.dead){
					var newDistPlayerX = ia.state.player.body.x - ia.monster.body.x;
					var newDirection = newDistPlayerX < 0 ? -1 : 1;
					if(direction == newDirection){
						var laser = LaserFactory.create(ia.monster, ia.state, ia.monster.body.x + 35 * ia.monster.scale.x, ia.monster.body.y);
						laser.shoot(ia.state.player.body.x, ia.state.player.body.y);
					}
					ia.lastShot = ia.state.game.time.now;
					ia.shooting = false;
				}
			}, 2000);
		}
		
		ia.canShoot = function(){
			return this.lastShot && (this.state.game.time.now - this.lastShot) > this.fireRate * 1000;
		}
		
		ia.startShooting = function(){
			this.monster.body.velocity.y = 0;
			this.shooting = true;
			flashShootAnimation(this.state, ia.monster, 20, 11);
		}
		
		return ia;
			
		function enterInRangeCallback(p2Body, body, fixture1, fixture2, begin) {
			if(!this.ia.playerInRange && p2Body === this.ia.state.player.body){
				this.ia.playerInRange = true;
				// Start moving animations
			}
		}
		
		
		function flashShootAnimation(state, monster,x, y) {
			// animation
			var flash = state.add.sprite(x , y, 'loading-flash');
			monster.addChild(flash);
			flash.anchor.set(0.5);
			flash.alpha = 0;
			flash.scale.x = 0.8;
			flash.scale.y = 0.8;
			var tween = state.add.tween(flash).to({alpha: 1, rotation: 50}, 2000)
			tween.onComplete.add(function(){
				flash.destroy();
				monster.ia.shootAnimation = null;
			});
			tween.start();
			monster.ia.shootAnimation = tween;
		}
	}
});
