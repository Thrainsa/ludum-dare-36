define(['consts','engine/save-state',  'engine/utils', 'entities/robot', 'entities/arrow'],
	   function(Consts, SaveState, Utils, RobotFactory, ArrowFactory) {

	return {
		preload: preload,
		create: create,
		destroy: destroy
	};

	function preload(state) {
		RobotFactory.preload(state);
	}
	
	function create(state, playerInfo) {
		var player = state.game.add.sprite(playerInfo.x, playerInfo.y, 'player-idle');
		
		if(Consts.DEBUG.SHOW_BODIES){
			player.point = new Phaser.Point(playerInfo.x, playerInfo.y);
		}
		
		// correction position body
		Utils.attachObjectToGrid(player,true, Consts.TILESIZE);
				
		// Physics
		state.game.physics.p2.enable(player, Consts.DEBUG.SHOW_BODIES);
		player.body.fixedRotation = true;
		
		// correction position body
		Utils.replaceSprite(player);
		player.body.y -= Consts.PLAYER.STANCES["idle"].body.offset.y - 5;
		
		player.info = {};
		player.info.state = state;
		player.info.points = SaveState.points || 0;
		player.group = state.game.add.group();
		player.group.add(player);
		
		if(playerInfo.properties.direction == "left"){
			player.scale.x *= -1;
		}
		
		// Keyboard binding
		
		// Define your actions
		var ACTION = { LEFT: 1,    JUMP: 2,    RIGHT: 3,    DOWN: 4};
		// Define your keymap, as many keys per action as we want
		var defaultKeymap = {    
			[ACTION.LEFT]:  [Phaser.KeyCode.A, Phaser.KeyCode.Q, Phaser.KeyCode.LEFT],    
			[ACTION.JUMP]:    [Phaser.KeyCode.W, Phaser.KeyCode.Z, Phaser.KeyCode.UP],    
			[ACTION.RIGHT]: [Phaser.KeyCode.D, Phaser.KeyCode.RIGHT],    
			[ACTION.DOWN]:  [Phaser.KeyCode.S, Phaser.KeyCode.DOWN]
		};
		// Create Keymap class
		var Keymap = function(keyboard, defaultKeymap) {
			this.map = {};
			var self = this;
			_.forEach(defaultKeymap, function(KeyCode, action) {
				self.map[action] = [];
				if (_.isArray(KeyCode)) {
					_.forEach(KeyCode, (code) => {
						self.map[action].push(keyboard.addKey(code));
					});
				} else {
					self.map[action].push(keyboard.addKey(KeyCode));
				}
			});
		};
		// isDown function for your action
		Keymap.prototype.isDown = function(action) {
			for (let i = 0, length = this.map[action].length; i < length; i++) {
				if (this.map[action][i].isDown) {
					return true;
				}
			}
			return false;
		}; // Create the Keymap
		var actionMap = new Keymap(state.game.input.keyboard, defaultKeymap); // In your update function you can now use
		player.actionMap = actionMap;
		
		// Mouse Input
		state.game.input.mouse.capture = true;
		player.leftButton = state.game.input.activePointer.leftButton;
		
		player.currentTerrain = null;
		player.isFalling = false;
		player.idle = false;
		
		// Add the robot
		player.robot = RobotFactory.create(state, player);
		state.displaygroups.objects.add(player.robot);
		
		state.game.input.onUp.add(function(event) {
			if(this.arrow && this.arrow.bendStartTime){
				player.switchStance(Consts.PLAYER.STANCES["shoot"]);
                player.animations.play("shoot", Consts.PLAYER.STANCES["shoot"].fps.shoot, false);
				player.arrow.shoot(event);
				player.arrow = false;				
			}
        }, player);
		
		state.game.input.addMoveCallback(move, player);
		
		player.update = function() {
			if(Consts.DEBUG.SHOW_BODIES){
				this.point.x = this.x;
				this.point.y = this.y;
				this.info.state.game.debug.geom(this.point, 'rgb(0,255,0)');
			}
			var result = this.touching();
			
			// Mouse Action
			if(player.leftButton.isDown && result.terrain != Consts.TERRAIN.AIR){
				this.body.velocity.x = 0;
				if(!this.arrow){
					this.arrow = ArrowFactory.create("arrow", this.info.state, this);
					this.arrow.bend();
					this.switchStance(Consts.PLAYER.STANCES["shoot"]);
					this.animations.play("bend", Consts.PLAYER.STANCES["shoot"].fps.bend, false);
					this.shooting = true;
					this.move = false;
				} else if(!this.move){
					move(player.leftButton.parent, 0 ,0,true, this);
				} else if(this.arrow.loaded && this.arrow.nextType && this.info.points >= 5){
					var nextType = this.arrow.nextType;
					this.arrow.destroy();
					this.arrow = ArrowFactory.create(nextType, this.info.state, this);
					this.arrow.bend();
					this.animations.play("bend-"+nextType, Consts.PLAYER.STANCES["shoot"].fps.bend, false);
					this.shooting = true;
					this.move = false;
				}
				// We don't want to do anything else if we bend the bow
				return;
			}
			
			// Keyboard Actions
			if (actionMap.isDown(ACTION.RIGHT)  && !result.right) {
				speedScale = 1;
			} else if (actionMap.isDown(ACTION.LEFT)  && !result.left) {
				speedScale = -1;
			} else{
				speedScale = 0;
			}
			
			this.body.velocity.x = Consts.SPEED * speedScale;
			
			if((this.body.velocity.x < 0 && this.scale.x > 0)
				|| this.body.velocity.x > 0 && this.scale.x < 0){
				this.scale.x *= -1;
				this.body.offset.x *= -1;
				this.body.x = this.body.x - 2 * (this.body.offset.x);
			}
			
			// Load Animation en fonction du terrain

			if (result.terrain != this.currentTerrain || (this.shooting && this.animations.currentAnim.isFinished) || (this.idle && Math.abs(this.body.velocity.x) > 0)) {
				this.shooting = false;
				this.idle = false;
                // Changement de terrain => Changement animation
                this.currentTerrain = result.terrain;
                if (result.down) {
					player.switchStance(Consts.PLAYER.STANCES["walk"]);
                    this.animations.play("walk", Consts.PLAYER.STANCES["walk"].fps, true);
                }
                else {
					player.switchStance(Consts.PLAYER.STANCES["jump"]);
                    if (this.body.velocity.y >= 0) {
                        this.animations.play("fall", Consts.PLAYER.STANCES["jump"].fps, false);
                        this.isFalling = true;
                    }
                    else {
                        this.animations.play("jump", Consts.PLAYER.STANCES["jump"].fps, true);
                        this.isFalling = false;
                    }
                }
            } else if (result.terrain == Consts.TERRAIN.AIR
                    && !this.isFalling && this.body.velocity.y >= 0) {
				this.idle = false;
                this.animations.play("fall", 10, false);
                this.isFalling = true;
				this.jumping = false;
            } else if(result.down && this.body.velocity.x == 0 && !this.idle){
				player.switchStance(Consts.PLAYER.STANCES["idle"]);
                this.animations.play("idle", Consts.PLAYER.STANCES["idle"].fps, true);
				this.idle = true;
			}

			// jump
			//this.body.damping = 0;
			if (actionMap.isDown(ACTION.JUMP) && result.down) {
				this.body.moveUp(Consts.JUMP_FORCE);
				this.animations.play("jump", Consts.PLAYER.STANCES["jump"].fps, false);
				this.jumping = true;
			} else if(this.jumping && result.terrain == Consts.TERRAIN.AIR && !actionMap.isDown(ACTION.JUMP)){
				this.body.velocity.y += (this.info.state.time.elapsed / 500) * Consts.JUMP_FORCE;
			}
		};
		
		player.touching = function() {
			var threshold = 0.5;
			var xAxis = p2.vec2.fromValues(1, 0);
			var yAxis = p2.vec2.fromValues(0, 1);
			var result = { left: false, right: false, down: false, terrain: Consts.TERRAIN.AIR };
			
			this.game.physics.p2.world.narrowphase.contactEquations.forEach(function(c) {
				var player = this;
				if (c.bodyA === player.body.data || c.bodyB === player.body.data) {
					var dx = p2.vec2.dot(c.normalA, xAxis);
					var dy = p2.vec2.dot(c.normalA, yAxis);
					if (c.bodyA === player.body.data) {
						dx *= -1;
						dy *= -1;
					}
					if (dx <= -threshold) { result.left = true; }
					if (dx > threshold) { result.right = true; }
					if (dy > threshold) {
						result.down = true;
						if (c.bodyA === player.body.data) {
							if(c.bodyB.parent.properties && c.bodyB.parent.properties.terrain){
								result.terrain = c.bodyB.parent.properties.terrain;
							}
						} else {
							if(c.bodyA.parent.properties && c.bodyA.parent.properties.terrain){
								result.terrain = c.bodyA.parent.properties.terrain;
							}
						}
					}
				}
			}, this);
			return result;
		};
		
		player.switchStance = function(stance) {
			if (stance != this.currentStance) {
				var direction = this.scale.x < 0 ? -1 : 1;
				this.currentStance = stance;

				var posX = this.body.x;
				var posY = this.body.y;
			
				this.loadTexture(stance.texture, 0, true);
				stance.animations.forEach(function(animation) {
					this.animations.add(animation.name, animation.frames || null);
				}, this);
		
				// Physics
				this.body.clearShapes();
				var chamfered = 20;
				var chamferedTop = 30;
				
				this.body.addPolygon( {} , 
					[
						[chamfered,0],
						[stance.body.width - chamfered, 0],
						[stance.body.width, chamferedTop],
						[stance.body.width, stance.body.height - chamferedTop],
						[stance.body.width - chamfered, stance.body.height],
						[chamfered, stance.body.height],
						[0, stance.body.height - chamferedTop],
						[0,chamferedTop]
					]);
				
				this.body.offset.x = stance.body.offset.x || 0;
				this.body.offset.y = stance.body.offset.y || 0;
				
				if(direction < 0){
					this.body.offset.x *= -1;
					this.body.x = this.body.x - 2 * (this.body.offset.x);
				}
				
				this.body.x = posX;
				this.body.y = posY;
				
				this.body.setCollisionGroup(state.groups.player);
				
				
				this.body.collides(state.groups.enemies);
				this.body.collides(state.groups.points);
				this.body.collides(state.groups.projectile);
				this.body.collides(state.groups.sensor);
				this.body.collides(state.groups.map);
				this.body.collides(state.groups.obstacles);
				
				this.body.data.gravityScale = stance.gravityScale || 1;
				
				if(stance.attachedAnimation){					
					this.attachedAnimation = state.game.add.sprite(this.x, this.y, stance.texture);
					this.group.add(this.attachedAnimation, false, 0);
					this.attachedAnimation.anchor.x = 0.5;
					this.attachedAnimation.anchor.y = 0.5;
					this.attachedAnimation.scale.x = this.scale.x;
					
					this.pivot.x = -15;
					this.body.offset.x -= 15 * direction;
					this.pivot.y = 9;
					this.body.offset.y += 9;
					
					playerDebug = this;
					stanceDebug = stance;
	
				} else if(this.attachedAnimation){
					this.pivot.y = 0
					this.pivot.x = 0;
					this.attachedAnimation.destroy();
					this.rotation = 0;
				}
			}
		}
		
		player.death = function(){
			this.dead = true;
			player.info.state.death();
		}
		
		player.wounded = function(){
			this.death();
		}
		
		player.collectPoints = function(point){
			this.info.points = Math.min(this.info.points + point.getValue(), 100);
			this.info.state.collectNewPoints(this.info.points);
			flashAnimation(this.info.state, point, 0.2);
			point.destroy();
		}
		player.useEnergy = function(value){
			this.info.points = Math.max(this.info.points - value, 0);
			this.info.state.collectNewPoints(this.info.points);
		}
		
		
		return player;
	}
	
	function collideWithEnemy(playerBody, enemyBody){
		if (enemyBody && enemyBody.sprite.death != true) {
			this.rotation = -1.7;
			this.info.state.add.tween(this)
				.to({rotation: -3.4, alpha: 0}, 500)
				.start();
			this.info.state.add.tween(this.body)
				.to({x: this.body.x - 100, y: this.body.y - 60}, 500)
				.start();
			this.animations.stop();
			this.death();
		}
	}	

	function flashAnimation(state, sprite, scale) {
		// animation
		if (!SaveState.mute) {
			state.add.sound('click').play();
		}

		var flash = state.add.sprite(sprite.cameraOffset.x, sprite.cameraOffset.y, 'flash');
		flash.fixedToCamera = true;
		
		flash.anchor.set(0.5);
		flash.scale.set(scale || 0.3);
		state.add.tween(flash)
			.to({alpha: 0, rotation: 50}, 500)
			.start();
	}

	function smokeAnimation(state, player) {
		if (!SaveState.mute) {
		//	state.add.sound('start).play();
		}
		var smoke = state.add.sprite(player.x, player.y, 'smoke');
		smoke.anchor.set(0.5);
		smoke.scale.set(player.width/smoke.width);
		state.add.tween(smoke)
			.to({alpha: 0, rotation: 50}, 300)
			.start();
		state.add.tween(smoke.scale)
			.to({x: smoke.scale.x * 1.5, y: smoke.scale.x * 1.5}, 300)
			.start();
	}
		

	function destroy(player) {
	};
	
	function move(pointer, x, y, isDown, player) {
		if(!player){
			player = this;
		}
		if(player.arrow && player.arrow.bendStartTime){
			player.move = true;
			var direction = player.scale.x < 0 ? -1 : 1;
			var xDiff = pointer.worldX - player.arrow.x;
			if(xDiff > -15 && xDiff < 15){
				xDiff = 15 * direction;
			}
			
			if(xDiff < 0 && player.scale.x > 0 || xDiff > 0 && player.scale.x < 0){
				var newDirection = -direction;
				player.scale.x *= -1;
				player.attachedAnimation.scale.x = player.scale.x;
				player.attachedAnimation.x += 2 *(15 * newDirection);
			}
			var yDiff = pointer.worldY - player.arrow.y;
			var angle = Math.atan2(yDiff, Math.abs(xDiff));
			if(xDiff < 0){
				angle = -angle;
			}
			player.rotation = angle;
		}
	}

});
