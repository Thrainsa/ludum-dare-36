define(['consts', 'engine/utils'], function(Consts, Utils) {
	return {
		create: create
	};

	function create(state, data) {
		var sensor = state.game.add.sprite(data.x, data.y, "sensor");
		
		state.game.physics.p2.enable(sensor, Consts.DEBUG.SHOW_BODIES);
		sensor.body.clearShapes();
		sensor.body.fixedRotation = true;
		sensor.body.static = true;
		sensor.data = data;
		sensor.state = state;
		
		// correction position body
		Utils.replaceObject(sensor,data.width,data.height);
		rangeSensor = sensor.body.addRectangle(data.width,data.height, 0, 0);
		rangeSensor.sensor = true;
		
		sensor.body.setCollisionGroup(state.groups.sensor);
		sensor.body.collides(state.groups.player);
		sensor.playerIn = false;

		return sensor;
	}

});
