define(['consts', 'engine/utils'], function(Consts, Utils) {
	return {
		create: create
	};

	function create(creator, state, x, y) {
		var laser = state.game.add.sprite(x, y, 'laser');
		state.displaygroups.objects.add(laser);
		laser.creator = creator;
		laser.info = {
			dommage: 10,
			state : state
		};
		laser.scale.x = creator.scale;
		laser.sensor = true;

		laser.update = function() {
			if(!this.collide){
				if(this.shot && this.body.velocity.x != 0 ){
					this.body.rotation = Math.atan2(this.body.velocity.y, this.body.velocity.x);
				}
			}
		};
		
		laser.shoot = function(targetX, targetY) {
			this.scale.x = 1;
			this.info.state.physics.p2.enable(laser, Consts.DEBUG.SHOW_BODIES);
		
			this.body.setCollisionGroup(state.groups.projectile);
			this.body.collides(state.groups.map, collideWithMap);
			this.body.collides(state.groups.enemies, collideWithEnemy);
			this.body.collides(state.groups.player, collideWithEnemy);
			
			this.body.damping = 0;
			this.body.data.gravityScale = 0;
			
			var force = Consts.LASER_FORCE;
			var xDiff = targetX - this.body.x;
			var yDiff = targetY - this.body.y;
			var angle = Math.atan2(yDiff, xDiff);
			
			laser.body.force.x = Math.cos(angle) * force;    // accelerateToObject 
			laser.body.force.y = Math.sin(angle) * force;
			laser.body.rotation = Math.atan2(laser.body.force.y, laser.body.force.x);
			
			this.shot = true;
		};

		return laser;
	}
	
	function collideWithMap(laserBody, collisionBody){
		laserBody.sprite.destroy();
	}
	
	function collideWithEnemy(laserBody, collisionBody){
		var laserSprite = laserBody.sprite;

		if(collisionBody.sprite != laserBody.sprite.creator){
			laserBody.sprite.destroy();
			if(collisionBody.sprite.wounded){
				collisionBody.sprite.wounded(laserSprite.info.dommage);
			}
		}
	}
	

});
