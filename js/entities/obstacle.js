define(['consts', 'engine/utils'], function(Consts, Utils) {
	return {
		create: create
	};

	function create(state, data) {
		var obstacle = state.game.add.sprite(data.x, data.y, data.properties.name);
		
		Utils.attachObjectToGrid(obstacle, true, Consts.TILESIZE);

		state.game.physics.p2.enable(obstacle, Consts.DEBUG.SHOW_BODIES);

		obstacle.body.fixedRotation = true;
		obstacle.properties = data.properties;
		
		// correction position body
		Utils.replaceSprite(obstacle, true, Consts.TILESIZE);
	
		var collisionRect = Consts.COLLISION[data.properties.name];
		obstacle.body.clearShapes();			
        obstacle.body.addRectangle(collisionRect.width,collisionRect.height,collisionRect.offset.x, collisionRect.offset.y);
		obstacle.collisionRect = collisionRect;
		
		
		obstacle.body.setCollisionGroup(state.groups.obstacles);
		obstacle.body.collides(state.groups.map);
		if(data.properties.open != "true"){
			obstacle.body.collides(state.groups.player);
			obstacle.body.collides(state.groups.projectile, collideWithProjectile);
			obstacle.frame = collisionRect.openframe;
			setTimeout(function(){
				obstacle.animations.add("close-"+obstacle.properties.name, obstacle.collisionRect.closeAnimation);
				obstacle.animations.play("close-"+obstacle.properties.name, 5, false);
			}, 2000);
			
		} else {
			obstacle.frame = collisionRect.openframe;
		}
		
		obstacle.body.kinematic = true;
		
		obstacle.info = {
			destructible: data.properties.destructible == "true" || false,
			small_passage: data.properties.small_passage == "true" || false
		}
		
		
		// Audio

		// IA
		obstacle.update = function() {
		};
		
		obstacle.explode = function(){
			// TODO 
			this.body.clearShapes();
			this.destroyed = true;
		}
		
		obstacle.allowPassageBelow = function(){
			// TODO 
			this.body.clearShapes();
			// this.body.kinematic = true;
		}
		return obstacle;
	}
	
	function collideWithProjectile(obstacleBody, projectileBody){
		if(projectileBody.sprite.dommage && obstacleBody.sprite.properties.power){
			if(projectileBody.sprite.dommage >= obstacleBody.sprite.properties.power){
				// ouvre la porte
				obstacleBody.sprite.animations.add("open-"+obstacleBody.sprite.properties.name, obstacleBody.sprite.collisionRect.openAnimation);
				obstacleBody.sprite.animations.play("open-"+obstacleBody.sprite.properties.name, 5, false);
				setTimeout(function(){
					obstacleBody.clearShapes();
				}, 1000);
			}
		}
		

	}
});
