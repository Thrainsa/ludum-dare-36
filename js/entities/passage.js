define(['consts', 'engine/utils', 'engine/save-state', 'entities/sensor'], function(Consts, Utils, SaveState, FactorySensor) {
	return {
		create: create
	};

	function create(state, data) {
		var passage = FactorySensor.create(state, data);
		passage.passing = false;
		
		passage.body.onBeginContact.add(function(){
			if(!passage.passing){
				passage.passing = true;
				if(this.data.properties.level){
					var level = this.data.properties.level;
				} else {
					var level = SaveState.level;
				}
				if(this.data.properties.sublevel){
					var subLevel = this.data.properties.sublevel;
				} else {
					var subLevel = 1;
				}
				SaveState.from = SaveState.level + "-" + SaveState.subLevel;
				this.state.changeMap(level, subLevel);
			}
		}, passage );

		return passage;
	}

});
