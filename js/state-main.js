define(['consts', 'state-level', 'engine/save-state', 'engine/forms', 'leaderboards', 'state-menu', 'state-cutscene', 'state-leaderboard'],
		function(Consts, StateLevel, SaveState, Forms, Leaderboards, StateMenu, StateCutscene, StateLeaderboard) {

	var SAVENAME = 'save1';

	var state = new Phaser.State();

	state.preload = function() {

		// Save state

        SaveState.load(Consts.SAVE_STATE_DEFAULT.gameName, Consts.SAVE_STATE_DEFAULT);

		// Register game states

		this.state.add('cutscene', StateCutscene);
		this.state.add('menu', StateMenu);
		this.state.add('level', StateLevel);
		this.state.add('leaderboard', StateLeaderboard);

		// Start

		if (Consts.DEBUG.MUTE) {
			SaveState.mute = true;
		}

		if (Consts.DEBUG.TEST_SUBMIT_LEADERBOARD != 0) {
			SaveState.points = Consts.DEBUG.TEST_SUBMIT_LEADERBOARD;
			this.state.start('leaderboard');
		}
		else if (!Consts.DEBUG.SKIP_MENU) {
			this.state.start('menu');
		}
		else if (!Consts.DEBUG.SKIP_INTRO_CUTSCENE) {
			this.state.start('cutscene');
		}
		else {
			if (Consts.DEBUG.TEST_COLLISION_MAP == true) {
				Consts.DEBUG.FORCE_LEVEL = '1-debug';
			}
			if (Consts.DEBUG.FORCE_LEVEL) {
				levelInfo = Consts.DEBUG.FORCE_LEVEL.split('-');
				SaveState.level = levelInfo[0];
				SaveState.subLevel = levelInfo[1];	
				SaveState.unlockedCharacters = parseInt(SaveState.level);
			}
			this.state.start('level');
		}

	};

	state.create = function() {

	};

	return state;

});
