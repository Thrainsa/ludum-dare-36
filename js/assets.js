define(['consts'], function(Consts) {

  return {
    images: [
		'flash',
		'loading-flash',
		'arrow',
		'arrow-energy',
		'laser',
		'sensor',
		'generator',
		[ 'UI/',
			'bgbottom',
			'bgtop',
			'logo',
			'key',
			'keysmall',
			'shutter',
			'energy_bar_background'
		]
    ],

    tilesets: [
      'tileset',
      'objects'
    ],

    audio: [
      'menu',
	  'cutscene',
	  'click',
	  'theme',
	  'theme-2',
	  'theme-Boss'
    ],

    spritesheet: [
      ['player-idle', 150, 188],
	  ['player-walk', 150, 188],
      ['player-jump', 150, 188],
	  ['player-shoot', 150, 188],
	  ['robot-idle', 54, 63],
	  ['robot-enemy-idle', 54, 63],
	  ['robot-zombie-enemy-idle', 150, 188],
	  ['robot-zombie-enemy-walk', 150, 188],
	  ['robot-zombie-enemy-shoot', 150, 188],
	  ['robot-zombie-enemy-activate', 150, 188],
	  ['robot-zombie-enemy-death', 150, 188],
	  ['energy_bar', 228,79],
	  ['energy-idle', 42,51],
	  ['gate-right', 288,288],
	  ['gate-left', 288,288]
    ]

  };

});
