define(['consts','engine/utils', 'engine/save-state', 'engine/tilemap', 'entities/player', 'entities/monster', 'entities/point', 'entities/secret', 'entities/passage','entities/obstacle', 'background', 'entities/shutter', 'engine/forms'],
	   function(Consts, Utils, SaveState, FactoryTilemap, FactoryPlayer, FactoryMonster, FactoryPoint, FactorySecret, FactoryPassage, FactoryObstacle, Background, Shutter, Forms) {

	var state = new Phaser.State();

	var text;

	var themeSound;

	state.preload = function() {
		if(!SaveState.from){
			SaveState.from = '0-0';
		}
		if(Consts.DEBUG.from){
			SaveState.from = Consts.DEBUG.from;
		}
		this.levelInfo = {};
		this.levelInfo.map;
		this.levelInfo.level = Consts.DEBUG.level ? Consts.DEBUG.level : SaveState.level;
		this.levelInfo.subLevel = Consts.DEBUG.subLevel ? Consts.DEBUG.subLevel : SaveState.subLevel;
		this.levelInfo.tilesets = ["tileset", "objects"];
		this.game.stage.backgroundColor = '#0a1012';
		var levelId = 'level' + this.levelInfo.level + '-' + this.levelInfo.subLevel;
		this.load.tilemap(levelId , 'tilemaps/maps/' + levelId + '.json', null, Phaser.Tilemap.TILED_JSON);
		this.waterFilter = state.add.filter('Water');
		this.bossFilter = state.add.filter('Boss');
		
		FactoryPlayer.preload(this);
	};
	
	state.create = function() {
		if (!themeSound || !themeSound.isPlaying) {
			if(this.levelInfo.level == 1){
				themeSound = state.add.audio('theme');
			} else if(this.levelInfo.level == 2){
				themeSound = state.add.audio('theme-2');
			} else if(this.levelInfo.level == 3){
				themeSound = state.add.audio('theme-Boss');
			} 
		}

		if (!themeSound.isPlaying && !SaveState.mute) {
			themeSound.play('', 0, 1, true);
		}

		this.game.physics.startSystem(Phaser.Physics.P2JS);
		this.game.physics.p2.setImpactEvents(true);

		this.groups = {
			map: this.game.physics.p2.createCollisionGroup(),
			player: this.game.physics.p2.createCollisionGroup(),
			robot: this.game.physics.p2.createCollisionGroup(),
			enemies: this.game.physics.p2.createCollisionGroup(),
			obstacles: this.game.physics.p2.createCollisionGroup(),
			points: this.game.physics.p2.createCollisionGroup(),
			projectile: this.game.physics.p2.createCollisionGroup(),
			sensor: this.game.physics.p2.createCollisionGroup()
		};
		
		this.sensors = [];

		this.bg = Background.create(this);

		// Chargement de la carte
		this.loadMap(this.levelInfo.level, this.levelInfo.subLevel);
		this.game.camera.follow(state.player);

		this.shutter = Shutter.create(this);

		this.game.physics.p2.pause();

		// Mute button
		var muteKey = state.add.image(Consts.WIDTH - 50, Consts.HEIGHT-60, 'keysmall');
		muteKey.fixedToCamera = true;
		var mText = state.add.text(Consts.WIDTH - 40, Consts.HEIGHT-53, 'M', { font: "14px sans-serif"});
		mText.fixedToCamera = true;
		var muteText = state.add.text(Consts.WIDTH - 90, Consts.HEIGHT-53, 'Mute', { font: "14px sans-serif"});
		muteText.fixedToCamera = true;
		var mKey = state.input.keyboard.addKey(Phaser.Keyboard.M);
		mKey.onDown.add(function() {
			SaveState.mute = !SaveState.mute;
			SaveState.save();
			if (SaveState.mute) {
				themeSound.volume = 0;
			}
			else {
				if(!themeSound.isPlaying){
					themeSound.play('', 0, 1, true);
				}
				themeSound.volume = 1;
			}
		});

		this.shutter.open(function(){
			var firstLevel = state.levelInfo.level == 1 && state.levelInfo.subLevel == 1;
			setTimeout(function() {
				state.game.physics.p2.resume();
				
				state.levelInfo.levelTxt = state.game.add.text(830, 25, "Level "+ state.levelInfo.level + '-' + state.levelInfo.subLevel, { font: "28px Comic Sans Ms", fill: "#000000", align: "right" });
				state.levelInfo.levelTxt.fixedToCamera = true;
				
				state.levelInfo.energyBarBg= state.game.add.sprite(25 + 57, 20 + 32, 'energy_bar_background');
				state.levelInfo.energyBarBg.fixedToCamera = true;
				state.levelInfo.energyBarBg.filters = [state.waterFilter];
				
				state.levelInfo.energyBar= state.game.add.sprite(25, 20, 'energy_bar');
				state.levelInfo.energyBar.fixedToCamera = true;
				state.collectNewPoints(state.player.info.points);
				
				state.levelInfo.escKey = state.game.input.keyboard.addKey(Phaser.Keyboard.ESC);
				state.levelInfo.pKey = state.game.input.keyboard.addKey(Phaser.Keyboard.P);
				state.levelInfo.qKey = state.game.input.keyboard.addKey(Phaser.Keyboard.Q);
				state.levelInfo.escKey.onDown.add(state.togglePause, state);
				state.levelInfo.pKey.onDown.add(state.togglePause, state);
				state.levelInfo.qKey.onDown.add(state.quitToMenu, state);
			}, firstLevel ? 1000 : 0);
		});
    };

	state.update = function() {
		this.waterFilter.update();
		this.bossFilter.update();
		this.bg.offset(this.game.camera.x, this.game.camera.y);
		
		if (this.player ) {
            this.player.update();

			// Mort lorsque l'on tombe dans le vide
			if ((this.player.body.x < this.game.camera.x || this.player.body.y > this.player.info.state.world.height) && !this.player.dead) {
                this.player.death();
            }
        }
		this.sensors.forEach(function(sensor, index) {
			sensor.update();
		});

    };

    state.render = function() {
    };
	
	state.death = function(){
		this.game.physics.p2.pause();
		SaveState.subLevel = SaveState.subLevel;
		SaveState.save();

		this.shutter.close(function(){
			state.game.state.start("level");
		});
	}
	
	state.collectNewPoints = function(points){
		this.levelInfo.energyBarBg.scale.x = Math.min(1, points/100);
		
		if(this.levelInfo.energyBar && points == 100){
			this.levelInfo.energyBar.frame = 1;
		}
	}

	state.shutdown = function(){
		FactoryPlayer.destroy(this.player);
	};
	
	state.changeMap = function(level, subLevel){
		if(themeSound.isPlaying){
			themeSound.fadeOut(1000);
		}
		SaveState.level = level;
		SaveState.subLevel = subLevel;
		
		SaveState.points = this.player.info.points;
		if(subLevel != 1){
			var hiddenLevel = level+"-"+subLevel;
			if (SaveState.hiddenVisited.indexOf(hiddenLevel) == -1){
				SaveState.hiddenVisited.push(hiddenLevel);
			}
		}
		SaveState.save();
		this.shutter.close(function(){
			this.isSwitchingLevels = false;
			if(level == 3){
				SaveState.cutscene = 'beforeBoss';
				state.game.state.start("cutscene");
			} else {
				state.game.state.start("level");
			}
		});
	}
	
	state.victory = function(){
		if(themeSound.isPlaying){
			themeSound.fadeOut(1000);
		}
		SaveState.victory = true;
		SaveState.save();
		this.shutter.close(function(){
			this.isSwitchingLevels = false;
			SaveState.cutscene = 'victory';
			state.game.state.start("cutscene");
		});
		
	}
	
	state.loadMap = function(levelId, checkPointId){
		var groupsToCollideWith = [
			state.groups.player,
			state.groups.enemies,
			state.groups.obstacles,
			state.groups.projectile
		];

		this.levelInfo.map = FactoryTilemap.load(this, 'level' + levelId + '-' + checkPointId, groupsToCollideWith);
		
		state.displaygroups = {};
		state.displaygroups.objects = state.add.group();

		for (var type in this.levelInfo.objects) {
			var objects = this.levelInfo.objects[type];
			for (var i=0; i < objects.length; i++) {
				var data = objects[i];
				if (type == "player") {
					if(!SaveState.from || SaveState.from == data.properties.from){
						state.player = FactoryPlayer.create(state, data);
					}
					// state.displaygroups.objects.add(state.player);
                } else if (type == "monster") {
                    var m = FactoryMonster.create(state, data);
					state.displaygroups.objects.add(m);
                } else if (type == "point") {
                    var p = FactoryPoint.create(state, data);
					state.displaygroups.objects.add(p);
                } else if (type == "obstacle") {
                    var o = FactoryObstacle.create(state, data);
					state.displaygroups.objects.add(o);
                } else if(type == "secret"){
					var secret = FactorySecret.create(state, data);
					this.sensors.push(secret);
				} else if(type == "passage"){
					FactoryPassage.create(state, data);
				}
			};
		}
		
		// Put layers needed on top
		for (var layerName in this.levelInfo.layers)
        {
			var layer = this.levelInfo.layers[layerName];
			if(layer.properties.top == "true"){
				this.game.world.bringToTop(layer);
			}
        }
	}
	
	state.togglePause = function(characterId) { // XXX other params are set on keydown, we have to check if the value is valid later on
		if (!this.player.dead) {
			if (!this.levelInfo.paused) {
				this.game.physics.p2.pause();
				this.levelInfo.paused = true;

				this.showPauseMenu();
				
				// Handle key to change menu
				
			} else {
				this.pauseCharacter = null;
				this.game.physics.p2.resume();
				this.levelInfo.paused = false;
				Forms.getActive().hide();
			}
		}
    };
	
	var currentVoice = null;
	state.showPauseMenu = function() {
		var formData = {};
		var form = Forms.load('pause', formData);
		form.show();
	}
	
	state.quitToMenu = function(){
		if (this.levelInfo.paused) {
			Forms.getActive().hide();
			state.sound.stopAll();
			state.game.state.start("menu");
		}
	}
	
	return state;

});
