define([], function() {

	return {

		DEBUG : {
			TEST_COLLISION_MAP: false,
			SKIP_INTRO_CUTSCENE: false,
			SKIP_MENU: false,
			MUTE: false,
			SHOW_BODIES: false,
			DISABLE_SHUTTER: false,
			TEST_SUBMIT_LEADERBOARD: 0,
			FORCE_LEVEL: null
		},
		
		PLAYER : {
			STANCES : {
				"idle": {
					texture: "player-idle",
					body: {
						width: 70,
						height: 140,
						offset: {
							x: 10,
							y: -19
						}
					},
					animations: [
						{name: "idle"}
					],
					fps: 2
				},
				"walk": {
					texture: "player-walk",
					body: {
						width: 70,
						height: 140,
						offset: {
							x: 10,
							y: -19
						}
					},
					animations: [
						{name: "walk"},
					],
					fps: 10
				},
				"jump": {
					texture: "player-jump",
					body: {
						width: 70,
						height: 140,
						offset: {
							x: 10,
							y: -19
						}
					},
					animations: [
						{name: "jump", frames : [0,1]},
						{name: "fall", frames : [2,3]}
					],
					fps: 10
				},
				"shoot": {
					texture: "player-shoot",
					body: {
						width: 70,
						height: 140,
						offset: {
							x: 10,
							y: -19
						}
					},
					animations: [
						{name: "legs", frames : [0]},
						{name: "bend", frames : [1,2]},
						{name: "bend-arrow-energy", frames : [3]},
						{name: "shoot", frames : [4, 5, 6]}
					],
					fps:{
						legs: 1,
						bend: 3,
						shot: 4
					},
					attachedAnimation : "legs"
				},
			}
		},
		
		ROBOT : {
			STANCES : {
				"idle": {
					texture: "robot-idle",
					body: {
						width: 43,
						height: 43,
						offset: {
							x: -1,
							y: -8
						}
					},
					polygon : [ [23,55], [7,38],[11,20], [25,18], [48,30.5], [50.5,46], [40,60.5]],
					animations: [
						{name: "idle"}
					]
				}
			}
		},
		ENEMY: {
			ROBOT : {
				STANCES : {
					"idle": {
						texture: "robot-enemy-idle",
						body: {
							width: 43,
							height: 43,
							offset: {
								x: -1,
								y: -8
							}
						},
						// polygon : [ [23,55], [7,38],[11,20], [25,18], [48,30.5], [50.5,46], [40,60.5]],
						animations: [
							{name: "idle"}
						],
						gravityScale: 0
					}
				}
			},
			ROBOT_ZOMBIE : {
				STANCES : {
					"idle": {
						texture: "robot-zombie-enemy-idle",
						body: {
							width: 85,
							height: 180,
							offset: {
							},
							chamfered : 30,
							chamferedTop: 40
						},
						animations: [
							{name: "idle"}
						],
						fps: 1
					},
					"walk": {
						texture: "robot-zombie-enemy-walk",
						body: {
							width: 100,
							height: 188,
							offset: {
							}
						},
						animations: [
							{name: "walk"}
						],
						fps: 5
					},
					"shoot": {
						texture: "robot-zombie-enemy-shoot",
						body: {
							width: 100,
							height: 188,
							offset: {
							}
						},
						animations: [
							{name: "prepare", frames : [0]},
							{name: "shoot", frames : [1]}
						],
						fps: 5
					},
					"activate": {
						texture: "robot-zombie-enemy-activate",
						body: {
							width: 100,
							height: 188,
							offset: {
							}
						},
						animations: [
							{name: "sleeping", frames : [0]},
							{name: "activate"}
						],
						fps: 4
					},
					"death": {
						texture: "robot-zombie-enemy-death",
						body: {
							width: 100,
							height: 188,
							offset: {
							}
						},
						animations: [
							{name: "death"},
						],
						fps: 4
					}
				}
			}
		},
		COLLISION: {
			'gate-right' : {
				width: 248,
				height: 288,
				offset: {
					x: -20
				},
				openframe: 0,
				closeframe: 4,
				openAnimation: [4,3,2,1,0],
				closeAnimation: [0,1,2,3,4]
			},
			'gate-left' : {
				width: 248,
				height: 288,
				offset: {
					x: 20
				},
				openframe: 4,
				closeframe: 0,
				openAnimation: [0,1,2,3,4],
				closeAnimation: [4,3,2,1,0]
			}
		},

		WIDTH: 960,
		HEIGHT: 600,
		TILESIZE: 96,

		SPEED: 600,
		GRAVITY: 2000,
		LASER_FORCE: 30 * 2000,

		PLAYER_X: 300,
		LATE_PLAYER_ACCELERATION: 0.3,

		JUMP_FORCE: 1200,
		
		TERRAIN: {
			GROUND: "ground",
			AIR: "air",
			WATER: "water"
		},
		
		LEVELS: {
			0: {
				SUBLEVELS: 1
			}
		},
		
		HTML: {
			KEYS: {
				1: "&#x21E7;",
				2: "&#x21E6;",
				3: "&#x21E8;",
				4: "&#x21E9;"
			}
		},
		SAVE_STATE_DEFAULT: {
			gameName: "save1",
        	playerName: "New player",
			level: 1,
			subLevel : 1,
			points: 0,
			hiddenVisited: [],
			cutscene: 'start'
        }
		
	};

});
