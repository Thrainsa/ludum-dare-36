define([], function() {

	return {
		attachObjectToGrid: attachObjectToGrid,
		replaceSprite: replaceSprite,
		replaceObject: replaceObject,
		switchStance: switchStance
	};

    function attachObjectToGrid(sprite, yGrid, tileHeight, xGrid, tileWidth) {
        xGrid = (xGrid !== undefined) ? xGrid : false;
        yGrid = (yGrid !== undefined) ? yGrid : false;
        
        if (xGrid) {
            sprite.position.x = Math.round(sprite.position.x / tileWidth) * tileWidth;
        }
        if (yGrid) {
            sprite.position.y = Math.round(sprite.position.y / tileHeight) * tileHeight;
        }
    }
	
	function replaceSprite(sprite) {
        sprite.body.x += sprite.width/2;
		sprite.body.y -= sprite.height/2;
    }
	
	function replaceObject(sprite,width, height) {
        sprite.body.x += width/2;
		sprite.body.y += height/2;
    }
	
	function switchStance(sprite, stance){
		var direction = sprite.scale.x < 0 ? -1 : 1;
		sprite.currentStance = stance;

		var posX = sprite.body.x;
		var posY = sprite.body.y;
	
		sprite.loadTexture(stance.texture, 0, true);
		stance.animations.forEach(function(animation) {
			sprite.animations.add(animation.name, animation.frames || null);
		}, sprite);

		// Physics
		sprite.body.clearShapes();
		if(stance.polygon){
			sprite.body.addPolygon( {} ,stance.polygon);
		} else if(stance.body.chamfered){
			var chamfered = stance.body.chamfered;
			var chamferedTop = stance.body.chamferedTop || stance.body.chamfered;
			
			sprite.body.addPolygon( {} , 
				[
					[chamfered,0],
					[stance.body.width - chamfered, 0],
					[stance.body.width, chamferedTop],
					[stance.body.width, stance.body.height - chamferedTop],
					[stance.body.width - chamfered, stance.body.height],
					[chamfered, stance.body.height],
					[0, stance.body.height - chamferedTop],
					[0,chamferedTop]
				]);
		} else {
			sprite.body.addRectangle(stance.body.width,stance.body.height, 0, 0);
		}
		
		sprite.body.offset.x = stance.body.offset.x || 0;
		sprite.body.offset.y = stance.body.offset.y || 0;
		
		if(direction < 0){
			sprite.body.offset.x *= -1;
			sprite.body.x = sprite.body.x - 2 * (sprite.body.offset.x);
		}
		
		sprite.body.x = posX;
		sprite.body.y = posY;

		if (stance.gravityScale !== undefined) {
            sprite.body.data.gravityScale = parseInt(stance.gravityScale);
        }
	}
});
