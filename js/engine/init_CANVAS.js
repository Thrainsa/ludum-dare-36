require.config({
	baseUrl: "js",
	paths: {
		json3: "lib/json3.min", // JStorage requires JSON2/3
		jquery: "../bower_components/jquery/dist/jquery.min", // Bootstrap & "forms" require jQuery
		underscore: "../bower_components/underscore/underscore-min", // Underscore (JS toolkit)
		jstorage: "lib/jstorage.min", // JStorage (local storage)
		bootstrap: "lib/bootstrap.min", // Bootstrap (UI)
		phaser: "../bower_components/phaser/build/phaser", // PhaserJS (game library)
		phaserTilemap: "lib/phaser.TilemapParser", // PhaserJS TilemapParser Custom
		optional: "lib/require-optional", // Support optional libraries flag in RequireJS
		base64: "lib/base64.min", // Base64 (encoding)
	}
});

require(['json3', 'jquery'], function() {
	require(['underscore', 'jstorage', 'bootstrap', 'phaser', 'optional'], function() {
		require(['phaserTilemap'], function() {
			require(['consts', 'engine/forms', 'engine/state-boot', 'engine/state-preloader', 'state-main', 'optional!consts-debug'],
			function(Consts, Forms, StateBoot, StatePreloader, StateMain) {
				Consts.mode = Phaser.CANVAS;
				new Phaser.Game(Consts.WIDTH, Consts.HEIGHT, Phaser.CANVAS, 'phaser', {
					create: function() {
						Forms.refresh();
						this.time.advancedTiming = true;
						
						this.state.add('boot', StateBoot);
						this.state.add('preloader', StatePreloader);
						this.state.add('main', StateMain);
	
						this.state.start('boot');
					}
				});
			});
		});
	});
});
